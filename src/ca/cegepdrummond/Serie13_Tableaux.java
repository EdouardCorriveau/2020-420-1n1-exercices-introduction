package ca.cegepdrummond;

import java.util.Scanner;

public class Serie13_Tableaux {
    /*
     * Modifiez le code afin de transférer les entiers d'un tableau dans un autre.
     *
     * Le code pour faire l'entrée des données dans le premier tableau vous est donné.
     * Vous devez transférer ce tableau dans le deuxième et afficher ce dernier en mettant
     * 1 élément par ligne.
     *
     * Le premier entier données en entré est le nombre d'éléments à mettre dans le tableau.
     *
     *
     * Exemple
     * 6 8 4 3 5 4 2
     * affichera:
     * 8
     * 4
     * 3
     * 5
     * 4
     * 2
     *
     */
    public void tableau1() {

        int[] tableau1 = remplirTableau();
        int nombreElement = tableau1.length;
        int[] tableau2 = new int[nombreElement];

        //votre code.
    }

    /**
     * Permet de remplir un tableau à partir d'une série de nombre entier.
     * Le premier entier indique combien d'éléments mettre dans le tableau.
     * @return le tableau d'entiers rempli.
     */
    private int[] remplirTableau() {
        Scanner s = new Scanner(System.in);

        int nombreElement = s.nextInt();
        int[] tableau = new int[nombreElement];
        int element;
        for (int i = 0; i < nombreElement; i++) {
            element = s.nextInt();
            tableau[i] = element;
        }
        return tableau;
    }

    /*
     * Modifiez le code afin que la méthode tableau2() affiche la valeur la plus grande du tableau d'entier.
     *
     * Le code pour faire l'entrée des données dans le tableau vous est donné (c'est la fonction remplirTableau ci-haut).
     * Le premier entier en entrée est le nombre d'élément du tableau.
     *
     * Afin de simplifier le code, le tableau doit avoir au moins 2 éléments.
     *
     * Exemple:
     *  6 4 5 2 7 3 1
     * affichera:
     * 7
     *
     */
    public void tableau2() {
        int[] tableau = remplirTableau();
        int max = tableau[0];
        for (int i = 1; i < tableau.length; i++) {
            //votre code
        }
        System.out.println(max);

    }

    /*
     * Modifiez le code afin que la méthode inverseTableau() inverse le table passé en paramètre.
     * Vous devez ensuite afficher ce tableau inversé sur une seule ligne avec les nombres séparés par un espace
     * et la ligne se terminant par un espace.
     *
     * La méthode inverseTableau() ne doit pas créer un nouveau tableau, elle doit inverser celui qui est envoyé.
     *
     * Le code pour faire l'entrée des données dans le tableau vous est donné.
     * Le premier entier est le nombre d'élément du tableau.
     *
     * Exemple:
     * 4 1 4 5 2
     * affichera:
     * "2 5 4 1 "   (sans les guillements, mais avec l'espace à la fin)
     */

    public void tableau3() {
        int[] tableau1 = remplirTableau();
        inverseTableau(tableau1);

        for (int i = 0; i < tableau1.length; i++) {
            System.out.print(tableau1[i] + " ");
        }
        System.out.println(""); // conserver cette ligne.
    }

    private static void inverseTableau(int[] t) {
        // vous avez déjà vu comment inverser 2 éléments... ca prend une variable intermédiaire
        // bouclez jusqu'à la moitié du tableau (voir note ci-bas)
        // mettre l'élément du début dans la variable intermédiaire
        // prendre l'élément de la fin et le mettre dans l'élément du début
        // mettre la variable intermédiaire dans l'élément de fin

        //note: il y a 2 cas: nombre pair d'éléments et nombre impair.
        //      si c'est pair, il y aura toujours une paire d'éléments à échanger
        //      mais si c'est impair, on fait quoi avec le dernier élément qui est au milieu?

        int intermediaire;
        for (int i = 0; i < t.length / 2; i++) {
            //votre code
        }

    }

    /*
     * Modifiez le code afin que la méthode totalTableau() retourne la somme des entiers se trouvant dans le
     * tableau passé en paramètre
     */
    public void tableau4() {
        int[] tableau = remplirTableau();
        System.out.println(totalTableau(tableau));
    }

    private int totalTableau(int[] t) {
        int total = 0;
        //votre code
        return total;
    }

    /*
     * Modifiez le code afin que la méthode additionneTableaux() retourne un tableau qui est la somme des
     * deux tableaux passés en paramètres.
     * Vous devez ensuite afficher ce tableau de totaux sur une seule ligne avec les nombres séparés par un espace
     * et la ligne se terminant par un espace.
     *
     * Pour simplifier le code, les 2 tableaux seront de même longueur.
     */

    public void tableau5() {
        int[] tableau1 = remplirTableau();
        int[] tableau2 = remplirTableau();
        /* enlever cette ligne de commentaire

        int[] resultat = ??? ; // appelez la fonction pour additionner les tableaux en passant les 2 tableaux

        for (int i = 0; i < resultat.length; i++) {
            System.out.print(resultat[i] + " ");
        }

        enlever cette ligne de commentaire */
        System.out.println(""); // conserver cette ligne.

    }

    /* enlever cette ligne de commentaire

    // ajouter la fonction additionneTableaux qui retourne un tableau d'entier,
    // et qui prend 2 tableaux d'entier en parametre
    public ??? additionneTableaux(???, ???) {
      // votre code.
    }

    enlever cette ligne de commentaire */



    /***********
     * Attention, ce numéro est plus avancé. Mais il vous est fortement recommandé de le faire!
     */

    /*
     * Modifiez ce code afin de créer un jeu de tictactoe.
     * Vous devez créer un tableau 3x3 qui contiendra les X et les O
     * Au début, le tableau contiendra le numéro de la cellule de 1 à 9
     * 123
     * 456
     * 789
     *
     * Vous devez ensuite demander le numéro de la cellule dans laquelle mettre le O ou le X en alternance.
     * Si l'usager demander une cellule déjà occupée, il doit recevoir le message "occupée".
     * Si elle est libre, insérez un O ou un X dans cette cellule et réafficher le tableau de jeu.
     * Vous n'avez pas à détecter s'il y a un gagnant (nous le ferrons plus tard durant la session).
     * Pour terminer, l'usager doit entrer 0 (zéro)
     *
     *
     */
    public void tictactoe() {

        Scanner s = new Scanner(System.in);
        boolean fin = false; //indique si la partie est terminée (l'usager a entré 0)
        String[][] tableau = {
                {"1", "2", "3"},
                {"4", "5", "6"},
                {"7", "8", "9"}};
        afficheTicTacToe(tableau);
        int position;
        int x, y;
        String OX = "O";  //est-ce les O ou les X qui joue
        /* enlever cette ligne de commentaire
        do {

            System.out.println("position pour les " + OX);
            position = s.nextInt();
            if (position == 0) {
                ??? // indiquez que la partie est terminée
                //indice: il y a une variable qui sert à ca.
            } else {
                x = (position - 1) / 3; //la ligne à changer dans le tableau
                y = (position - 1) % 3; //la colonne à changer sur la ligne

                if( ??? ){ // vérifier si le tableau à la position x, y est libre
                        //   indice: il ne faut pas que ca soit un "O" ou un "X"

                    ??? //si c'est libre, mettre cette case à "O" ou "X"
                        //   indice: il y a une variable indiquant qui joue

                    ??? //et il faut maintenant changer le joueur
                        // indice: il y a une variable indiquant qui joue. Si c'est X mettre O, et vice-versa

                } else {
                    //si ce n'est pas libre, afficher le message "occupée"
                    System.out.println("occupée");
                }

                ??? // afficher tout le tableau
                // indice: il y a une fonction pour ca.
            }
        } while ( ??? ); //tant que la partie n'est pas terminée
                         // indice: il y a une variable qui indique si on a fini.
                enlever cette ligne de commentaire */

    }

    /**
     * affiche un tableau bi-dimensionnelle ligne par ligne.
     * @param ttt le tableau à afficher.
     */
    public static void afficheTicTacToe(String[][] ttt) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(ttt[i][j]);
            }
            System.out.println();
        }
    }
}
